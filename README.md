# Rapid Reader

**_ Rapid Reader is based on a fork of the dev branch of the
[OHIF Viewer XNAT Plugin](https://bitbucket.org/icrimaginginformatics/ohif-viewer-xnat-plugin/src/dev/)
repository._** The rapid-reader-bone is the main branch of the rapid viewer.

### Prerequisite

[ohif-viewer-XNAT-plugin 3.0](https://bitbucket.org/icrimaginginformatics/ohif-viewer-xnat-plugin/src/dev/)
should be installed on the XNAT 1.8 node

# Clone the frontend repo and build

git clone https://bitbucket.org/xnatdev/rapid-viewer cd rapid-viewer yarn

# Assuming that XNAT URL is http://localhost:8080, run rapid-reader

yarn dev:rapid

# if you want to build

yarn build:rapid

# Create Work list

curl -u username:password -X POST [xnat_url]/xapi/workLists/ -H 'Content-Type:
application/json' -d '{"readerUsername":"uat_reader2","dueDate":
1617987219201,"name":"test","description":"test", "reportId":"RPTBONE",
"status":"Open"}'

# Create Work items (You can run the following commands as many as you want with different session info)

curl -u username:password -X POST [xnat_url]/xapi/workLists/1/items -H
'Content-Type: application/json' -d '{"experimentId":"MIRRIR_E00076"}'

**_--------------------_**

# Original OHIF Viewer README:

Please refer to [OHIF Viewer 2.0](https://github.com/OHIF/Viewers)

# OHIF Viewer XNAT README:

Please refer to
[OHIF Viewer XNAT Plugin](https://bitbucket.org/icrimaginginformatics/ohif-viewer-xnat-plugin/src/dev/)
