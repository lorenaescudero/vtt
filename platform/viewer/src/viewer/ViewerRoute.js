import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { useHistory } from 'react-router-dom';
import { useModal, useSnackbarContext } from '@ohif/ui';

import {
  WorkListStatus,
  WorkItemStatus,
  SurveyTypes,
  UserExpiredException,
  isWorkItemFinished,
  SurveyComponent,
  ReportComponent,
} from '@ohif/extension-rapid-reader';
import ConnectedXNATStandaloneRouting from '../connectedComponents/ConnectedXNATStandaloneRouting';

import WorkItemNotFound from './WorkItemNotFound';
import LoadingText from './LoadingText';
import SurveyText from './SurveyText';
import HeartBeat from '../components/HeartBeat.js';

function ViewerRoute({
  workListId,
  xnatUrl,
  workItems,
  currentWorkItemIdx,
  currentWorkItemId,
  readerFullName,
  workListStatus,
  times,
  evaluating,
  clearUser,
  setWorkList: keepWorkList,
  setWorkItemId,
  setReportForm,
  setWorkItemStatus,
  fetchWorkList,
  fetchRadReportForm,
  fetchSurveyTemplateQuestions,
  fetchSurvey,
  fetchPostSurvey,
  fetchUploadPdf,
  fetchBeginTimeTrack,
}) {
  const [loading, setLoading] = useState({ isLoading: false, error: false });
  const [reasonRequired, setReasonRequired] = useState(false);
  const [signRequired, setSignRequired] = useState(false);
  const [surveyRequired, setSurveyRequired] = useState(false);
  const [surveyType, setSurveyType] = useState(SurveyTypes.PreSurvey);
  const [surveyQuestions, setSurveyQuestions] = useState([]);

  const { show: showModal, hide: hideModal } = useModal();
  const history = useHistory();
  const snackbar = useSnackbarContext();

  useEffect(() => {
    const refreshWorkList = async () => {
      let hasError = false;
      try {
        setLoading({ isLoading: true, error: false });
        setWorkItemStatus('');

        // Fetch the selected workList
        const workList = await fetchWorkList(xnatUrl, workListId);
        keepWorkList(workList);
        const items = workList.items;
        if (!items.length) {
          return;
        }

        // Show the first non-finished work item
        const firstWorkItemIdx = items.findIndex(
          item => !isWorkItemFinished(item.status)
        );
        if (firstWorkItemIdx >= 0) {
          setWorkItemId(items[firstWorkItemIdx].id);
        } else {
          setWorkItemId(items[items.length - 1].id);
        }

        // Retrieve the report form and keep it
        const { reportId } = workList;
        const radReportData = await fetchRadReportForm(reportId);
        setReportForm(reportId, radReportData);

        // Retrieve the survey form
        const { surveyTemplateId } = workList;
        if (!surveyTemplateId) {
          setSurveyRequired(false);
        } else {
          // Check if the pre-survey was already taken
          setSurveyType(SurveyTypes.PreSurvey);
          const survey = await fetchSurvey(
            xnatUrl,
            workListId,
            SurveyTypes.PreSurvey
          );
          setSurveyRequired(!survey);

          // Retrieve and keep the survey template questions
          const questions = await fetchSurveyTemplateQuestions(
            xnatUrl,
            surveyTemplateId
          );
          setSurveyQuestions(questions);
        }
      } catch (error) {
        hasError = true;
        if (error instanceof UserExpiredException) {
          clearUser();
        } else {
          throw error;
        }
      } finally {
        setLoading({ isLoading: false, error: hasError });
      }
    };

    refreshWorkList();
  }, [
    workListId,
    xnatUrl,
    clearUser,
    fetchWorkList,
    fetchRadReportForm,
    setReportForm,
    setWorkItemId,
    setWorkItemStatus,
    fetchSurvey,
    fetchSurveyTemplateQuestions,
    fetchUploadPdf,
    keepWorkList,
    fetchBeginTimeTrack,
  ]);

  useEffect(() => {
    if (!workListId || !times) {
      return;
    }

    async function timeMgmt() {
      if (!times.length) {
        await fetchBeginTimeTrack(xnatUrl, workListId);
        const workList = await fetchWorkList(xnatUrl, workListId);
        keepWorkList(workList);
        return;
      }
    }

    timeMgmt();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [times, workListId, evaluating]);

  useEffect(() => {
    // Show the Post-Survey button once all the work items have been evaluated.
    const postSurvey = async () => {
      if (surveyQuestions.length === 0) {
        return;
      }

      if (!workItems || workItems.length <= 0) {
        return;
      }

      // Check if there is an open work item
      const completed =
        workItems.findIndex(item => item.status === WorkItemStatus.Open) === -1;
      if (!completed) {
        return;
      }

      // Retrieve the Post-Survey information
      const survey = await fetchSurvey(
        xnatUrl,
        workListId,
        SurveyTypes.PostSurvey
      );
      if (survey) {
        return;
      }
      setSurveyType(SurveyTypes.PostSurvey);
      setSurveyRequired(true);
    };
    postSurvey();
  }, [fetchSurvey, surveyQuestions.length, workItems, workListId, xnatUrl]);

  useEffect(() => {
    setSignRequired(workListStatus === WorkListStatus.Reviewing);
  }, [workListStatus]);

  if (loading.error) {
    return <div>Error: {JSON.stringify(loading.error)}</div>;
  } else if (loading.isLoading) {
    return <LoadingText />;
  }

  if (workItems.length === 0 || currentWorkItemIdx < 0) {
    return <WorkItemNotFound />;
  }

  const currentWorkItem = workItems[currentWorkItemIdx];
  const {
    projectId,
    subjectId,
    experimentId,
    experimentLabel,
  } = currentWorkItem;

  const handleBeginSurvey = surveyType => {
    showModal({
      content: SurveyComponent,
      contentProps: {
        handleSubmit: async form => {
          await fetchPostSurvey(xnatUrl, workListId, surveyType, form);
          setSurveyRequired(false);
          if (surveyType === SurveyTypes.PostSurvey) {
            setSignRequired(true);
          }
        },
        closeModal: hideModal,
        questions: surveyQuestions,
        submitButtonText:
          surveyType === SurveyTypes.PreSurvey
            ? 'Submit and Begin Evaluation'
            : 'Submit',
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title: surveyType,
    });
  };

  const handleBeginSignReport = () => {
    showModal({
      content: ReportComponent,
      contentProps: {
        handleSubmit: async file => {
          try {
            await fetchUploadPdf(xnatUrl, workListId, file);
            setSignRequired(false);
            snackbar.show({
              title: 'Success',
              message: 'Successfully signed',
              position: 'topRight',
              autoClose: true,
            });
            history.push('/');
          } catch (error) {
            snackbar.show({
              title: 'Error while sigining',
              message: error.message,
              position: 'topRight',
              type: 'error',
              autoClose: true,
            });
          }
        },
        closeModal: hideModal,
        xnatUrl: xnatUrl,
        readerFullName: readerFullName,
        workListId: workListId,
        finishedDate: new Date(),
        workItems: workItems,
        uploadPdf: fetchUploadPdf.bind(undefined, xnatUrl, workListId),
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title: 'Signature',
    });
  };

  return (
    <>
      {surveyRequired && (
        <SurveyText
          handleBegin={handleBeginSurvey.bind(undefined, surveyType)}
          text={`Begin ${surveyType}`}
        />
      )}
      {!surveyRequired && signRequired && (
        <SurveyText
          handleBegin={handleBeginSignReport}
          text="Sign the Summary Report"
        />
      )}
      <ConnectedXNATStandaloneRouting
        key={`${workListId}_${currentWorkItemId}`}
        workListId={workListId}
        projectId={projectId}
        subjectId={subjectId}
        experimentId={experimentId}
        experimentLabel={experimentLabel}
      ></ConnectedXNATStandaloneRouting>
      <HeartBeat />
    </>
  );
}

ViewerRoute.propTypes = {
  history: PropTypes.object.isRequired,
  workListId: PropTypes.string.isRequired,
  xnatUrl: PropTypes.string.isRequired,
  workItems: PropTypes.array,
  currentWorkItemIdx: PropTypes.number.isRequired,
  currentWorkItemId: PropTypes.number.isRequired,
  readerFullName: PropTypes.string.isRequired,
  workListStatus: PropTypes.string.isRequired,
  times: PropTypes.array,
  evaluating: PropTypes.bool,
  clearUser: PropTypes.func.isRequired,
  setWorkList: PropTypes.func.isRequired,
  setWorkItemId: PropTypes.func.isRequired,
  setReportForm: PropTypes.func.isRequired,
  setWorkItemStatus: PropTypes.func.isRequired,
  fetchWorkList: PropTypes.func.isRequired,
  fetchRadReportForm: PropTypes.func.isRequired,
  fetchSurveyTemplateQuestions: PropTypes.func.isRequired,
  fetchSurvey: PropTypes.func.isRequired,
  fetchPostSurvey: PropTypes.func.isRequired,
  fetchUploadPdf: PropTypes.func.isRequired,
  fetchBeginTimeTrack: PropTypes.func.isRequired,
  fetchUpdateTimeTrack: PropTypes.func.isRequired,
};

export default withRouter(ViewerRoute);
