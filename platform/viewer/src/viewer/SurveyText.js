import React from 'react';
import PropTypes from 'prop-types';
import './SurveyText.styl';

function SurveyText({ handleBegin, text }) {
  return (
    <div className="overlay">
      <div className="overlay-content">
        <button className="surveyBtn" onClick={handleBegin}>
          {text}
        </button>
      </div>
    </div>
  );
}

SurveyText.propTypes = {
  handleBegin: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};
export default SurveyText;
