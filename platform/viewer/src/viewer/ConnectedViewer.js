import { connect } from 'react-redux';
import {
  actions,
  fetchWorkList,
  fetchRadReportForm,
  fetchSurveyTemplateQuestions,
  fetchSurvey,
  fetchPostSurvey,
  fetchUploadPdf,
  fetchBeginTimeTrack,
  fetchUpdateTimeTrack,
} from '@ohif/extension-rapid-reader';

import getXnatUrl from '../utils/getXnatUrl';
import ViewerRoute from './ViewerRoute.js';

const mapStateToProps = state => {
  return {
    xnatUrl: getXnatUrl(),
    workItems: state.rapidReader.filteredItems || [],
    currentWorkItemIdx: state.rapidReader.currentWorkItemIdx,
    currentWorkItemId: state.rapidReader.currentWorkItemId,
    readerFullName:
      state.authentication.user.firstName +
      (state.authentication.user.lastName
        ? ' ' + state.authentication.user.lastName
        : ''),
    workListStatus: state.rapidReader.workList.status,
    times: state.rapidReader.workList.times,
    evaluating: state.rapidReader.workList.evaluating,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clearUser: () => {
      dispatch(actions.clearUser());
    },
    setWorkList: items => {
      dispatch(actions.setWorkList(items));
    },
    setWorkItemId: currentWorkItemId => {
      dispatch(actions.setWorkItemId(currentWorkItemId));
    },
    setReportForm: (reportId, formData) => {
      dispatch(actions.setReportForm(reportId, formData));
    },
    setWorkItemStatus: status => {
      dispatch(actions.setWorkItemStatus(status));
    },
    fetchWorkList,
    fetchRadReportForm,
    fetchSurveyTemplateQuestions,
    fetchSurvey,
    fetchPostSurvey,
    fetchUploadPdf,
    fetchBeginTimeTrack,
    fetchUpdateTimeTrack,
  };
};

const ConnectedViewer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewerRoute);

export default ConnectedViewer;
