import ConnectedCurrentWorkItemToolbarComponent from './components/ConnectedCurrentWorkItemToolbarComponent';
import ConnectedTimeWatchToolbarComponent from './components/ConnectedTimeWatchToolbarComponent';
import ConnectedIssuesToolbarComponent from './components/ConnectedIssuesToolbarComponent';

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  SET_TOOL_ACTIVE: 'setToolActive',
  BUILT_IN: 'builtIn',
};

const definitions = [
  //   {
  //     id: 'prevWorkItem',
  //     label: 'Prev',
  //     icon: 'rapid-prev',
  //     type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //     commandName: 'rapidViewerMoveToPrevItem',
  //   },

  {
    id: 'workListTimeWatch',
    label: 'WorkListTimeWatch',
    icon: 'inline-edit',
    CustomComponent: ConnectedTimeWatchToolbarComponent,
  },
  {
    id: 'currentWorkItem',
    label: 'CurrentWorkItem',
    icon: 'inline-edit',
    CustomComponent: ConnectedCurrentWorkItemToolbarComponent,
  },
  {
    id: 'rapidComment',
    label: 'Issues',
    icon: 'inline-edit',
    CustomComponent: ConnectedIssuesToolbarComponent,
  },
  //   {
  //     id: 'nextWorkItem',
  //     label: 'Next',
  //     icon: 'rapid-next',
  //     type: TOOLBAR_BUTTON_TYPES.COMMAND,
  //     commandName: 'rapidViewerMoveToNextItem',
  //   },
];

export default {
  definitions,
  defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE',
};
