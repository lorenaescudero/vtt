import { doPost, doGet, doPut } from './util';

export async function fetchRadReportForm(reportId) {
  if (reportId === 'RPTBONE') {
    return MOCK_RAD_REPORT_FORM;
  }
  const url = `https://phpapi.rsna.org/radreport/v1/templates/${reportId}/details`;
  const response = await fetch(url, { method: 'GET' });

  const json = await response.json();
  return json.DATA;
}

const RPTBONE = `<!DOCTYPE html>
<html>
    <head>
        <title>Simple Bone Age Study</title>
        <meta charset="UTF-8" />
        <meta name="dcterms.identifier" content="cea97390-04db-4630-bfcf-3092353acccc" />
        <meta name="dcterms.title" content="Simple Bone Age Study" />
        <meta name="dcterms.description" content="This a simple bone age study template that just have two fields - age and month" />
        <meta name="dcterms.type" content="IMAGE_REPORT_TEMPLATE" />
        <meta name="dcterms.language" content="en" />
        <meta name="dcterms.publisher" content="Washington University in St. Louis" />
        <meta name="dcterms.rights" content="May be used freely, subject to license agreement" />
        <meta name="dcterms.license" content="http://www.radreport.org/license.pdf" />
        <meta name="dcterms.date" content="2021-03-08" />
        <meta name="dcterms.creator" content="Woonchan Cho" />
        <script type="text/xml">
            <template_attributes>
                <coded_content>
                    <coding_schemes>
                        <coding_scheme name="RADLEX" designator="2.16.840.1.113883.6.256"></coding_scheme>
                        <coding_scheme name="LOINC" designator="2.16.840.1.113883.6.1"></coding_scheme>
                    </coding_schemes>
                    <entry origtxt="procedureInformation">
                        <term>
                            <code meaning="Current Imaging Procedure Description" value="55111-9" scheme="LOINC"></code>
                        </term>
                    </entry>
                    <entry origtxt="findings">
                        <term>
                            <code meaning="Procedure Findings" value="59776-5" scheme="LOINC"></code>
                        </term>
                    </entry>
                    <entry origtxt="comparisons">
                        <term>
                            <code meaning="Radiology Comparison Study" value="18834-2" scheme="LOINC"></code>
                        </term>
                    </entry>
                    <entry origtxt="impression">
                        <term>
                            <code meaning="Impressions" value="19005-8" scheme="LOINC"></code>
                        </term>
                    </entry>
                    <entry origtxt="clinicalInformation">
                        <term>
                            <code meaning="Clinical Information" value="55752-0" scheme="LOINC"></code>
                        </term>
                    </entry>
                </coded_content>
            </template_attributes>
        </script>
    </head>
    <body>
        <section id="procedureInformation" class="level1" data-section-name="Procedure Information">
            <header class="level1">
                Procedure Information
            </header>
        </section>
        <section id="clinicalInformation" class="level1" data-section-name="Clinical Information">
            <header class="level1">
                Clinical Information
            </header>
        </section>
        <section id="comparisons" class="level1" data-section-name="Comparison">
            <header class="level1">
                Comparison
            </header>
        </section>
        <section id="findings" class="level1" data-section-name="Findings">
            <header class="level1">
                Findings
            </header>
            <p title="">
                <label for="NUMBER_1615226470740">Year</label>
                <span>
                    <input type="number" step="1.0" data-field-units="Year" id="TBONE_1" name="Year" data-field-type="NUMBER" data-field-completion-action="PROHIBIT" value="0" />
                </span>
            </p>
            <p title="">
                <label for="SELECTION_LIST_1615226470747">Months</label>
                <select id="TBONE_2" name="month" data-field-type="SELECTION_LIST" data-field-completion-action="PROHIBIT" value="0">
                    <option id="SELECTION_LIST_1615226470747_0" value="0" name="SELECTION_LIST_1615226470747" selected="">0</option>
                    <option id="SELECTION_LIST_1615226470747_1" value="3" name="SELECTION_LIST_1615226470747">3</option>
                    <option id="SELECTION_LIST_1615226470747_2" value="6" name="SELECTION_LIST_1615226470747">6</option>
                    <option id="SELECTION_LIST_1615226470747_3" value="9" name="SELECTION_LIST_1615226470747">9</option>
                </select>
            </p>
        </section>
        <section id="impression" class="level1" data-section-name="Impression">
            <header class="level1">
                Impression
            </header>
        </section>
    </body>
</html>

`;

const MOCK_RAD_REPORT_FORM = {
  dataType: 'html',
  templateData: RPTBONE,
  title: 'Bone Age',
};

export async function fetchPostAssessor(
  xnatUrl,
  projectId,
  subjectId,
  experimentId,
  username,
  reportName,
  assessorLabel,
  fields,
  csrfToken
) {
  const url = `${xnatUrl}/data/projects/${projectId}/subjects/${subjectId}/experiments/${experimentId}/assessors?inbody=true&XNAT_CSRF=${csrfToken}`;

  const xml = constructAssessorXml(
    projectId,
    experimentId,
    username,
    reportName,
    assessorLabel,
    fields
  );
  const response = await doPost(url, xml);
  return await response.text();
}

function constructAssessorXml(
  projectId,
  experimentId,
  username,
  reportName,
  assessorLabel,
  fields
) {
  let detail = '';
  fields.forEach(field => {
    detail += `<rad:${field.name}>${field.value}</rad:${field.name}>`;
  });
  const xml = `<rad:${reportName} xmlns:rad="http://nrg.wustl.edu/rad" xmlns:xnat="http://nrg.wustl.edu/xnat" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" project="${projectId}" label="${assessorLabel}">
<xnat:imageSession_ID>${experimentId}</xnat:imageSession_ID>
<rad:reader>${username}</rad:reader>${detail}
</rad:${reportName}>`;
  return xml;
}
export async function fetchPutAssessor(
  xnatUrl,
  projectId,
  subjectId,
  experimentId,
  assessorId,
  username,
  reportName,
  assessorLabel,
  fields,
  csrfToken
) {
  const url = `${xnatUrl}/data/projects/${projectId}/subjects/${subjectId}/experiments/${experimentId}/assessors/${assessorId}?inbody=true&XNAT_CSRF=${csrfToken}`;

  const xml = constructAssessorXml(
    projectId,
    experimentId,
    username,
    reportName,
    assessorLabel,
    fields
  );
  const response = await doPut(url, xml);
  return await response.text();
}

export async function fetchPostAssessment(
  xnatUrl,
  workListId,
  workItemId,
  reportId,
  assessorId,
  fields
) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/items/${workItemId}/assessment`;
  const response = await doPost(url, {
    reportId,
    assessorId,
    fields,
  });
  return await response.json();
}

export async function fetchUpdateWorkItemStatus(
  xnatUrl,
  workListId,
  workItemId,
  status,
  reason
) {
  const url = `${xnatUrl}/xapi/workLists/${workListId}/items/${workItemId}/status/${status}?failReason=${reason}`;
  const response = await doPut(url);
  return await response.json();
}

export async function fetchGetAssessment(
  xnatUrl,
  projectId,
  subjectId,
  experimentId,
  assessorLabel
) {
  const url = `${xnatUrl}/data/projects/${projectId}/subjects/${subjectId}/experiments/${experimentId}/assessors/${assessorLabel}?format=json`;
  const response = await doGet(url);
  return await response.json();
}
