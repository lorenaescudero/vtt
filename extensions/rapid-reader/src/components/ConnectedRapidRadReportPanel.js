import { connect } from 'react-redux';
import { withModal } from '@ohif/ui/src/contextProviders/ModalProvider';
import actions from '../redux/actions';
import {
  fetchPostAssessment,
  fetchGetAssessment,
  fetchPostAssessor,
  fetchPutAssessor,
  fetchUpdateWorkItemStatus,
} from '../fetch/radReport';
import {
  fetchUpdateWorkListStatus,
  fetchBeginTimeTrack,
  fetchUpdateTimeTrack,
  fetchWorkList,
} from '../fetch/workList';
import { WorkListStatus, WorkItemStatus } from '../constants';
import { isWorkItemFinished, constructAssessmentAssessorLabel } from '../utils';
import RapidRadReportPanel from './RapidRadReportPanel';
import { HttpException } from '../exception';

const mapStateToProps = state => {
  const { rapidReader, authentication } = state;
  const reportId = rapidReader.workList.reportId;
  const filteredStatus = rapidReader.workItemStatus;
  const {
    id: workListId,
    status: workListStatus,
    formDisabledWhenComplete,
    requiredFieldIds: requiredFieldIds,
    evaluating: evaluating,
    times: times,
  } = rapidReader.workList;
  const items = rapidReader.filteredItems;
  const currentWorkItemIdx = rapidReader.currentWorkItemIdx;
  const {
    id: workItemId,
    status: workItemStatus,
    projectId,
    subjectId,
    experimentId,
    reportAssessorId,
  } = items[currentWorkItemIdx];
  const { xnatUrl, username, csrfToken } = authentication.user;

  // TODO: parameterize reportName
  const reportName = 'BoneAgeAssessment';

  const _fetchGetAssessment = async () => {
    const res = await fetchGetAssessment.call(
      this,
      xnatUrl,
      projectId,
      subjectId,
      experimentId,
      constructAssessmentAssessorLabel(workListId, username, experimentId)
    );

    if (
      !res ||
      !res.items ||
      res.items.length === 0 ||
      !res.items[0].data_fields
    ) {
      return undefined;
    }

    return res.items[0].data_fields;
  };

  const _getExistingAssessorId = async () => {
    try {
      const res = await _fetchGetAssessment();
      return res.ID;
    } catch (error) {
      return reportAssessorId;
    }
  };

  const _fetchUpdateWorkItemStatus = async (workItemStatus, failReason) => {
    const res = await fetchUpdateWorkItemStatus(
      xnatUrl,
      workListId,
      workItemId,
      workItemStatus,
      failReason
    );

    if (
      workListStatus === WorkListStatus.Complete &&
      workItemStatus === WorkItemStatus.Open
    ) {
      rapidReader.workList.status = WorkListStatus.Partial;
      // dispatch(actions.setWorkItem(workList));
    }

    return res;
  };

  return {
    workListStatus: workListStatus,
    workItemStatus: workItemStatus,
    radReportData: rapidReader.reports[reportId],
    formDisabled:
      workListStatus === WorkListStatus.Complete ||
      workListStatus === WorkListStatus.Cancelled ||
      (formDisabledWhenComplete &&
        (workItemStatus === WorkItemStatus.Complete ||
          workItemStatus === WorkItemStatus.Cancelled ||
          workItemStatus === WorkItemStatus.Failed)),
    hasSubmittedAssessment: !!reportAssessorId,
    filteredStatus: filteredStatus,
    requiredFieldIds: requiredFieldIds,
    evaluating: evaluating,
    times: times,
    fetchPostAssessment: async fields => {
      try {
        let assessorId = undefined;

        // Retrieve the assessor id for this assessment to make sure that assessor is not deleted in the XNAT.
        const existingAssessorId = await _getExistingAssessorId();
        if (!existingAssessorId) {
          const res = await fetchPostAssessor(
            xnatUrl,
            projectId,
            subjectId,
            experimentId,
            username,
            reportName,
            constructAssessmentAssessorLabel(
              workListId,
              username,
              experimentId
            ),
            fields,
            csrfToken
          );
          if (res.indexOf('/assessors/')) {
            assessorId = res.substring(
              res.indexOf('/assessors/') + '/assessors/'.length
            );
          }
        } else {
          const newAssessorId = await fetchPutAssessor(
            xnatUrl,
            projectId,
            subjectId,
            experimentId,
            existingAssessorId,
            username,
            reportName,
            constructAssessmentAssessorLabel(
              workListId,
              username,
              experimentId
            ),
            fields,
            csrfToken
          );
          assessorId = newAssessorId;
        }

        return await fetchPostAssessment.call(
          this,
          xnatUrl,
          workListId,
          workItemId,
          reportId,
          assessorId,
          fields
        );
      } catch (error) {
        if (error instanceof HttpException) {
          if (error.body.indexOf('h3') > 0) {
            throw new Error(error.body.split('<h3>')[1].split('</h3>')[0]);
          }
        }
        throw error;
      }
    },

    fetchUpdateWorkListStatus: fetchUpdateWorkListStatus.bind(
      this,
      xnatUrl,
      workListId
    ),
    fetchUpdateWorkItemStatus: _fetchUpdateWorkItemStatus,
    fetchGetAssessment: _fetchGetAssessment,
    isWorkListFinished: () => {
      return !items.some(item => !isWorkItemFinished(item.status));
    },
    isLastItem: () => currentWorkItemIdx === items.length - 1,
    fetchBeginTimeTrack: fetchBeginTimeTrack.bind(this, xnatUrl, workListId),
    fetchUpdateTimeTrack: fetchUpdateTimeTrack.bind(this, xnatUrl, workListId),
    fetchWorkList: fetchWorkList.bind(this, xnatUrl, workListId),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    moveToNextWorkItem: () => {
      dispatch(actions.moveToNextWorkItem());
    },
    setWorkItem: workItem => {
      dispatch(actions.setWorkItem(workItem));
    },
    setWorkList: workList => {
      dispatch(actions.setWorkItem(workList));
    },
  };
};

const ConnectedRapidRadReportPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(RapidRadReportPanel);

export default withModal(ConnectedRapidRadReportPanel);
