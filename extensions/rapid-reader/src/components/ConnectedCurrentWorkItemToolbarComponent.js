import { connect } from 'react-redux';
import { withSnackbar } from '@ohif/ui/src/contextProviders/SnackbarProvider';
import CurrentWorkItemToolbarComponent from './CurrentWorkItemToolbarComponent';

const mapStateToProps = state => {
  const {
    rapidReader: { filteredItems = [], currentWorkItemIdx },
  } = state;

  return {
    filteredItems,
    currentWorkItemIdx,
  };
};

const ConnectedCurrentWorkItemToolbarComponent = connect(
  mapStateToProps,
  null
)(CurrentWorkItemToolbarComponent);

export default withSnackbar(ConnectedCurrentWorkItemToolbarComponent);
