import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './SurveyComponent.styl';
import { useEffect } from 'react';

const radios = [
  { value: 1, text: 'None' },
  { value: 2, text: 'Slight' },
  { value: 3, text: 'Moderate' },
  { value: 4, text: 'Severe' },
];

export function SurveyComponent({
  handleSubmit,
  closeModal,
  questions,
  submitButtonText,
}) {
  const [loading, setLoading] = useState(false);
  const [answers, setAnswers] = useState([]);

  useEffect(() => {
    setAnswers(questions.map(_ => undefined));
  }, [questions]);

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);

    try {
      const answer = answers.join('_$_');
      await handleSubmit({ answer });
      setLoading(false);
      closeModal();
    } catch (error) {
      setLoading(false);
    }
  }

  function validateForm() {
    return answers.findIndex(answer => answer === undefined) === -1;
  }

  function handleChange(idx, value) {
    const newAnswers = [...answers];
    newAnswers[idx] = value;
    setAnswers(newAnswers);
  }

  return (
    <div className="rapid-survey-form">
      <form onSubmit={onSubmit}>
        {questions.map((item, idx) => (
          <FormField key={item.id} title={item.question}>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              {radios.map(options => (
                <React.Fragment key={options.value}>
                  <div
                    style={{ display: 'flex', flexDirection: 'row' }}
                    onClick={e => {
                      handleChange(idx, options.value);
                    }}
                  >
                    <input
                      type="radio"
                      id={`${idx}_${options.value}`}
                      name={`${idx}`}
                      value={options.value}
                    />
                    <label htmlFor={`${idx}_${options.value}`}>
                      {options.text}
                    </label>
                  </div>
                </React.Fragment>
              ))}
            </div>
          </FormField>
        ))}

        <div className="footer">
          <button
            type="submit"
            className="submit-btn"
            disabled={loading || !validateForm()}
          >
            {submitButtonText ? submitButtonText : 'Submit'}
          </button>
        </div>
      </form>
    </div>
  );
}

SurveyComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  questions: PropTypes.array.isRequired,
  submitButtonText: PropTypes.string,
};

function FormField({ title, children, error }) {
  return (
    <label>
      <span>{title}</span>
      {children}
      {error && <p className="rapid-error">{error}</p>}
    </label>
  );
}

FormField.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  error: PropTypes.string,
};

export default SurveyComponent;
