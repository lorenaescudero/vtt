import React from 'react';
import PropTypes from 'prop-types';
import ToolbarButton from '@ohif/ui/src/viewer/ToolbarButton';
import { withModal } from '@ohif/ui/src/contextProviders/ModalProvider';
import { withSnackbar } from '@ohif/ui/src/contextProviders/SnackbarProvider';
import IssuesComponent from './IssuesComponent';
import { fetchCreateComment } from '../fetch/comment';
import { CommentStatus } from '../constants';

const title = 'Issues';

export function IssuesToolbarComponent({
  xnatUrl,
  fromEmail,
  workItem,
  button,
  modal,
  snackbarContext,
}) {
  const workItemId = workItem.id;

  async function handleSend({ toEmails, subject, body }) {
    try {
      const comment = await fetchCreateComment(xnatUrl, {
        workItemId,
        toEmails,
        subject,
        body,
      });
      if (comment.status !== CommentStatus.Sent) {
        throw new Error(`Failed to send the email: ${comment.status}`);
      }
      snackbarContext.show({
        title: 'Email has successfully been sent!',
      });
    } catch (error) {
      snackbarContext.show({
        title: error.message,
        type: 'error',
      });
    }
  }

  function handleClick() {
    modal.show({
      content: IssuesComponent,
      contentProps: {
        fromEmail,
        handleSend,
        closeModal: modal.hide,
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title,
    });
  }

  return (
    <ToolbarButton
      key={button.id}
      label={button.label}
      icon={button.icon}
      onClick={handleClick}
      isActive={false}
    />
  );
}

IssuesToolbarComponent.propTypes = {
  xnatUrl: PropTypes.string.isRequired,
  fromEmail: PropTypes.string.isRequired,
  workList: PropTypes.object.isRequired,
  workItem: PropTypes.object.isRequired,
  fetchCreateComment: PropTypes.func.isRequired,
  button: PropTypes.object.isRequired,
  modal: PropTypes.object.isRequired,
  snackbarContext: PropTypes.object.isRequired,
};

export default withSnackbar(withModal(IssuesToolbarComponent));
